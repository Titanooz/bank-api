package main
import(
	"flag"
	"database/sql"
	_"github.com/go-sql-driver/mysql"
	"github.com/gin-gonic/gin"
	"net/http"
	"fmt"
	"strconv"

)
type User struct {
	ID        int `json:"id"`
	FirstName string `json:"first_name"`
	LastName string `json:"last_name"`
}
type BankAccount struct {
	ID            int `json:"id"`
	UserID        string `json:"user_id"`
	AccountNumber int `json:"account_number"`
	Name          string `json:"name"`
	Balance       int `json:"balance"`
}
type AccountService interface {
	AllAccount(id int) ([]BankAccount, error)
	Insert(bankAcc *BankAccount) error
	DeleteByAccountID(id int) error
	Withdraw(bankAcc *BankAccount) error
	Deposite(bankAcc *BankAccount) error
}
type UserService interface {
	AllUser() ([]User, error)
	GetByID(id int) (*User, error)
	Insert(user *User) error
	DeleteByID(id int) error
	Update(user *User) (*User, error)
}
type Server struct {
	userService   UserService
	accountService AccountService
}

type UserServiceImp struct {
	db *sql.DB
}
type AccountServiceImp struct {
	db *sql.DB
}

func (s * AccountServiceImp) AllAccount(id int) ([]BankAccount,error){
	rows, err := s.db.Query("SELECT user_id,account_number,name,balance FROM accounts WHERE user_id = ? ",id)
	if err != nil {
		return nil, err
	}
	bas := []BankAccount{}
	for rows.Next() {
		var ba BankAccount
		err := rows.Scan(&ba.UserID, &ba.AccountNumber, &ba.Name,&ba.Balance)
		if err != nil {
			return nil, err
		}
		bas = append(bas, ba)
	}
	return bas, nil
}
func (s *AccountServiceImp) Insert(bankAcc *BankAccount) error {
	row := s.db.QueryRow("INSERT INTO accounts (user_id,account_number,name,balance) values (?, ?,?,?)",
	 bankAcc.UserID, bankAcc.AccountNumber,bankAcc.Name,bankAcc.Balance)

	if err := row.Scan(&bankAcc.ID); err != nil {
		return err
	}
	return nil
}
func (s *AccountServiceImp) DeleteByAccountID(id int) error {
	stmt := "DELETE FROM accounts WHERE id = ? "
	_, err := s.db.Exec(stmt, id)
	if err != nil {
		return err
	}
	return nil
}
func (s *AccountServiceImp) Withdraw(bankAcc *BankAccount)  error {
	row := s.db.QueryRow("UPDATE accounts SET balance = balance - ? WHERE id = ?", bankAcc.Balance, bankAcc.ID)
	if err := row.Scan(&bankAcc.ID); err != nil {
		return err
	}
	return nil
}
func (s *AccountServiceImp) Deposite(bankAcc *BankAccount)  error {
	row := s.db.QueryRow("UPDATE accounts SET balance = balance + ? WHERE id = ?", bankAcc.Balance, bankAcc.ID)
	if err := row.Scan(&bankAcc.ID); err != nil {
		return err
	}
	return nil
}

/////////////user/////////////////
func (s * UserServiceImp) AllUser() ([]User,error){
	rows, err := s.db.Query("SELECT id,first_name,last_name FROM users")
	if err != nil {
		return nil, err
	}
	uss := []User{}
	for rows.Next() {
		var us User
		err := rows.Scan(&us.ID, &us.FirstName, &us.LastName)
		if err != nil {
			return nil, err
		}
		uss = append(uss, us)
	}
	return uss, nil
}

func (s *UserServiceImp) GetByID(id int) (*User, error) {
	stmt := "SELECT id,first_name,last_name FROM users WHERE id = ?"
	row := s.db.QueryRow(stmt, id)
	var us User
	err := row.Scan(&us.ID, &us.FirstName, &us.LastName)
	if err != nil {
		return nil, err
	}
	return &us, nil
}
func (s *UserServiceImp) Insert(user *User) error {
	row := s.db.QueryRow("INSERT INTO users (first_name,last_name) values (?, ?)", user.FirstName, user.LastName)

	if err := row.Scan(&user.ID); err != nil {
		return err
	}
	return nil
}
func (s *UserServiceImp) Update(user *User) (*User,error){
	row := s.db.QueryRow("UPDATE users SET first_name = ? ,last_name = ?  WHERE id = ? ", user.FirstName, user.LastName,user.ID)
	if err := row.Scan(&user.ID); err != nil {
		return nil,err
	}
	return s.GetByID(user.ID)
}
func (s *UserServiceImp) DeleteByID(id int) error {
	stmt := "DELETE FROM users WHERE id = ? "
	_, err := s.db.Exec(stmt, id)
	if err != nil {
		return err
	}
	return nil
}


///////////////////////////Server////////////////////////////////////
func (s *Server) AllUser(c *gin.Context) {
	users, err := s.userService.AllUser()
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"object":  "error",
			"message": fmt.Sprintf("db: query error: %s", err),
		})
		return
	}
	c.JSON(http.StatusOK, users)
}
func (s *Server) GetByID(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))
	us, err := s.userService.GetByID(id)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, us)
}
func (s *Server) CreateUser(c *gin.Context) {
	var user User
	if err := c.ShouldBindJSON(&user); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, err)
		return
	}
	if err := s.userService.Insert(&user); err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusCreated, user)
}
func (s *Server) Update(c *gin.Context) {
	var user User
	if err := c.ShouldBindJSON(&user); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, err)
		return
	}
	us, err := s.userService.Update(&user)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, us)
}
func (s *Server) DeleteByID(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))
	if err := s.userService.DeleteByID(id); err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, err)
		return
	}
}
func (s *Server) AllAccount(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))
	bankAccs, err := s.accountService.AllAccount(id)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"object":  "error",
			"message": fmt.Sprintf("db: query error: %s", err),
		})
		return
	}
	c.JSON(http.StatusOK, bankAccs)
}
func (s *Server) CreateAccount(c *gin.Context) {
	var bankAccs BankAccount
	bankAccs.UserID = c.Param("id")
	if err := c.ShouldBindJSON(&bankAccs); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, err)
		return
	}
	if err := s.accountService.Insert(&bankAccs); err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusCreated, bankAccs)
}
func (s *Server) DeleteAccountByID(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))
	if err := s.accountService.DeleteByAccountID(id); err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, err)
		return
	}
}
func (s *Server) Withdraw(c *gin.Context) {
	var bankAccs BankAccount
	if err := c.ShouldBindJSON(&bankAccs); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, err)
		return
	}
	id, _ := strconv.Atoi(c.Param("id"))
	bankAccs.ID = id
    err := s.accountService.Withdraw(&bankAccs)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK,nil)
}
func (s *Server) Deposite(c *gin.Context) {
	var bankAccs BankAccount
	if err := c.ShouldBindJSON(&bankAccs); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, err)
		return
	}
	id, _ := strconv.Atoi(c.Param("id"))
	bankAccs.ID = id
    err := s.accountService.Deposite(&bankAccs)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK,nil)
}

func setupRoute(s *Server) *gin.Engine {
	r := gin.Default()
	auth := r.Group("/",gin.BasicAuth(gin.Accounts{
			"admin":"tree",
		}))

	auth.GET("/users", s.AllUser)
	auth.GET("/users/:id", s.GetByID)
	auth.POST("/users",s.CreateUser)
	auth.PUT("/users",s.Update)
	auth.DELETE("/users/:id",s.DeleteByID)
	auth.GET("/users/:id/bankAccounts",s.AllAccount)
	auth.POST("/users/:id/bankAccounts",s.CreateAccount)
	auth.DELETE("/bankAccounts/:id",s.DeleteAccountByID)
	auth.PUT("/bankAccounts/:id/withdraw",s.Withdraw)
	auth.PUT("/bankAccounts/:id/Deposite",s.Deposite)

	return r
}

func main(){
	host := flag.String("host","localhost","descript host")
	port := flag.String("port","8443","descript port")
	
	flag.Parse()
	addr := fmt.Sprintf("%s:%s",*host,*port)

	db, _ := sql.Open("mysql", "root:root@tcp(127.0.0.1:3306)/bank-api")
	defer db.Close()

	s := &Server{
		userService: &UserServiceImp{
			db: db,
		},
		accountService: &AccountServiceImp{
			db: db,
		},
	}

	r := setupRoute(s)
	r.Run(addr)
}
